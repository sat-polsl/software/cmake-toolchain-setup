cmake_minimum_required(VERSION 3.16)

macro(validate_selected_mcu)
    set(cortex-m0 stm32f0 stm32l0 stm32g0)
    set(cortex-m3 stm32f1 stm32f2 stm32l1)
    set(cortex-m4 stm32f3 stm32f4 stm32l4)
    set(cortex-m7 stm32h7 stm32f7)
    set(MCU_CORES cortex-m0 cortex-m3 cortex-m4 cortex-m7)
    foreach (MCU_CORE IN LISTS MCU_CORES)
        list(APPEND SUPPORTED_MCUS ${${MCU_CORE}})
    endforeach ()

    set(MCU "" CACHE STRING "Target MCU of the project")
    if (NOT MCU IN_LIST SUPPORTED_MCUS)
        message(FATAL_ERROR "Specified MCU is not supported.\n"
            "To properly run this project use cmake -D MCU=<mcu>.\n"
            "Currently these ones are supported: ${SUPPORTED_MCUS}")
    endif ()

    set(FLOAT_ABI soft CACHE STRING "Floating point ABI")
    foreach (MCU_CORE IN LISTS MCU_CORES)
        if (MCU IN_LIST ${MCU_CORE})
            set(TARGET_CORE ${MCU_CORE} CACHE STRING "Target MCU core")
            if (${MCU_CORE} STREQUAL cortex-m4 OR ${MCU_CORE} STREQUAL cortex-m7)
                set(FLOAT_ABI hard CACHE STRING "Floating point ABI" FORCE)
                if (${MCU_CORE} STREQUAL cortex-m4)
                    set(FPU -mfpu=fpv4-sp-d16 CACHE STRING "Floating point hardware flag")
                elseif (${MCU} STREQUAL stm32f7)
                    set(FPU -mfpu=fpv5-sp-d16 CACHE STRING "Floating point hardware flag")
                elseif (${MCU} STREQUAL stm32h7)
                    set(FPU -mfpu=fpv5-d16 CACHE STRING "Floating point hardware flag")
                endif ()
            endif ()
            break()
        endif()
    endforeach()
endmacro(validate_selected_mcu)

macro(fix_cmake_defaults)
    # Fix harmful CMake defaults
    # Fix pthreads linkage on MacOS
    IF (APPLE)
        set(CMAKE_THREAD_LIBS_INIT "-lpthread")
        set(CMAKE_HAVE_THREADS_LIBRARY 1)
        set(CMAKE_USE_WIN32_THREADS_INIT 0)
        set(CMAKE_USE_PTHREADS_INIT 1)
        set(THREADS_PREFER_PTHREAD_FLAG ON)
    ENDIF ()
endmacro(fix_cmake_defaults)

macro(setup_toolchain)
    validate_selected_mcu()
    fix_cmake_defaults()

    if (DEFINED ARM_TOOLCHAIN)
        set(CMAKE_PROGRAM_PATH ${ARM_TOOLCHAIN})
        message(${BOLD_YELLOW} "Toolchain directory: ${ARM_TOOLCHAIN}" ${NO_COLOR})
    else()
        message(${BOLD_YELLOW} "Using global toolchain" ${NO_COLOR})
    endif()

    include(toolchain)
endmacro(setup_toolchain)

